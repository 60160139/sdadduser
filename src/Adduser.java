import java.util.Scanner;
import java.util.ArrayList;
public class Adduser {
	static Scanner kb = new Scanner(System.in);
	static ArrayList<AddData> Data = new ArrayList<AddData>();
	static String Username;
	static String Password;
	static String Name;
	static String Surname;
	static String Tel;
	static int Height;
	static int Weight;
	public static void main(String[] args) {
		addUsername();
		
	}
	public static  void addUsername() {
		
		System.out.print("Username : ");
		Username = kb.next();
		System.out.println();
		if(checkUserName(Username)) {
			addPassword();
		}
		if(checkUserName(Username)) {
			
			addUsername();
		}
	}
	public static boolean checkUserName(String Username) {
		if(!(Username.equals(" "))) {
			return true;
		}
			printErrorUsername();
			return false;
		
	}
	public static void printErrorUsername() {
		System.out.print(" Username already exists in System ");
		System.out.println();
	}
	public static void addPassword() {
		System.out.print("Password : ");
		Password = kb.next();
		System.out.println();
		addName();
	}
	public static void addName() {
		System.out.print("Name : ");
		Name = kb.next();
		System.out.println();
		addSurname();
	}
	public static void addSurname() {
		System.out.print("Surname : ");
		Surname = kb.next();
		System.out.println();
		addTel();
	}
	public static void addTel() {
		System.out.print("Tel . : ");
		Tel = kb.next();
		System.out.println();
		addHeight();
	}
	public static void addHeight() {
		System.out.print("Height : ");
		Height = kb.nextInt();
		System.out.println();
		if(checkHeight(Height)) {
			addWeight();
		}else {
			addHeight();
		}
		
	}
	public static boolean checkHeight(int Height) {
		if(Height > 0) {
			return true;
		}else {
			printErrorH();
			return false;
		}
	}
	public static void printErrorH() {
		System.out.print("Invalid Height .Try again . Number");
		System.out.println();
	}
	public static void addWeight() {
		System.out.print("Weight : ");
		Weight = kb.nextInt();
		System.out.println();
		if(checkHeight(Weight)) {
			checkSave();
		}else {
			addWeight();
		}
	}
	public boolean checkWeight(int Weight) {
		if(Weight > 0) {
			return true;
		}else {
			printErrorW();
			return false;
		}
	}
	public void printErrorW() {
		System.out.println("Invalid Weight .Try again . Number");
		
	}
	
	public static void checkSave() {
		char num ;
		System.out.println(" Do you want to save Y or N . Y = Yes , N = No");
		System.out.print("Please input : ");
		num = kb.next().charAt(0);
		if(num == 'Y') {
			Data.add(new AddData(Username,Password,Name,Surname,Tel,Height,Weight));
			for (AddData i : Data) {
				System.out.print(i.getUsername()+i.getPassworde()+i.getName()+i.getSurname()+i.getTel()+i.getHeight()+i.getWeight());
				menu();
			}
			
		}else if(num =='N') {
			menu();
		}
		
		
	}
	public static void menu() {
		
	}
	
}
class AddData{
	String Username;
	String Password;
	String Name;
	String Surname;
	String Tel;
	int Height,Weight;
	
	public AddData(String username2, String password2, String name2, String surname2, String tel2, int height2, int weight2) {
		Username = username2;
		Password =password2;
		Name = name2;
		Surname = surname2 ;
		Tel = tel2;
		Height = height2;
		Weight = weight2;
	}
	public String getUsername() {
		return Username;
		
	}
	public String getPassworde() {
		return Password;
		
	}
	public String getName() {
		return Name;
		
	}
	public String getSurname() {
		return Surname;
		
	}
	public String getTel() {
		return Tel;
		
	}
	
	public int getHeight() {
		return Height;
		
	}
	public int getWeight() {
		return Weight;
		
	}
}

